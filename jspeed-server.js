var config;
try {
    config = require('./configs/config_dev.json');
} catch (e) {
    config = require('./configs/config.json');
}


var server = require('http').createServer();
var fileSystem = require('fs');
var path = require('path');
var url = require('url');
var stream = require("stream");
var net = require('net');
var Q = require("q");

var util = require("util");

function BufferStream(source) {

    if (!Buffer.isBuffer(source)) {

        throw(new Error("Source must be a buffer."));

    }

    // Super constructor.
    stream.Readable.call(this);

    this._source = source;

    // I keep track of which portion of the source buffer is currently being pushed
    // onto the internal stream buffer during read actions.
    this._offset = 0;
    this._length = source.length;

    // When the stream has ended, try to clean up the memory references.
    this.on("end", this._destroy);

}

util.inherits(BufferStream, stream.Readable);


// I attempt to clean up variable references once the stream has been ended.
// --
// NOTE: I am not sure this is necessary. But, I'm trying to be more cognizant of memory
// usage since my Node.js apps will (eventually) never restart.
BufferStream.prototype._destroy = function () {

    this._source = null;
    this._offset = null;
    this._length = null;

};


// I read chunks from the source buffer into the underlying stream buffer.
// --
// NOTE: We can assume the size value will always be available since we are not
// altering the readable state options when initializing the Readable stream.
BufferStream.prototype._read = function (size) {
    // If we haven't reached the end of the source buffer, push the next chunk onto
    // the internal stream buffer.
    if (this._offset < this._length) {

        this.push(this._source.slice(this._offset, (this._offset + size)));

        this._offset += size;

    }

    // If we've consumed the entire source buffer, close the readable stream.
    if (this._offset >= this._length) {

        this.push(null);

    }

};

var logger = require('./log.js');

var port = 8888;
if (process.argv.indexOf("-port") != -1) {
    port = process.argv[process.argv.indexOf("-port") + 1];
} else {
    logger.error('You need to add -port');
    process.exit(0);
}

var binDir = __dirname + '/bin';
var files = {
    'latency.bin': 8,
    '10mb.bin': 10,
    '25mb.bin': 25,
    '50mb.bin': 50,
    '100mb.bin': 100,
    '250mb.bin': 250,
    '500mb.bin': 500,
    '1000mb.bin': 1000,
}

var pattern = new RegExp(/^\/files/);
;

var WebSocketServer = require('ws').Server
        , wss = new WebSocketServer({server: server});

server.on('request', function (request, response) {
    response.statusCode = 200;
    response.end('It Works! JSpeedtest.net');
});
server.listen(port, function () {
    console.log('Listening on ' + server.address().port)
});

var util = require('util');
var events = require('events');

function formatBytes(bytes, decimals) {
    if (bytes == 0)
        return '0 Byte';
    var k = 1000; // or 1024 for binary
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
function BandwidthSampler(ws, interval) {
    interval = interval || 2000;
    var previousByteCount = 0;
    var self = this;
    var intervalId = setInterval(function () {
        var byteCount = ws.bytesReceived;
        var recv = byteCount - previousByteCount;
        var bytesPerSec = (byteCount - previousByteCount) / (interval / 1000);
        previousByteCount = byteCount;
//        console.log(bytesPerSec);
        self.emit('sample', recv);
    }, interval);
    ws.on('close', function () {
        clearInterval(intervalId);
    });
}
util.inherits(BandwidthSampler, events.EventEmitter);
var clients = [];
function a(e) {
    var t = new ArrayBuffer(e),
            n = new Uint8Array(t);
    n[0] = 85, n[1] = 80, n[2] = 76, n[3] = 79, n[4] = 65, n[5] = 68, n[6] = 32;
    for (var i = 7; e > i; i++)
        n[i] = Math.floor(100 * Math.random() + 20);
    return t
}

var sendBytes = a(1e6)
wss.on('connection', function (ws) {
    var address = ws.upgradeReq.connection.remoteAddress;
    var indexOfColon = address.lastIndexOf(':');
    var IP = address.substring(indexOfColon + 1, address.length);

    var ports = [20, 21, 22, 23, 24, 80, 8080, 8888, 9100];

    var url = ws.upgradeReq.url;
    if (url === '/latency') {
        ws.on('message', function (message) {
            ws.send(message, {mask: false});
        });
    }

    if (url === '/download') {
        ws.on('message', function (message) {
            var message = message.split(' ');
            if (message[0] === 'PING') {
                ws.send('PONG');
            } else {
                ws.send(sendBytes.slice(0, message[1]));
            }

        });
    }

    if (url === '/upload') {
        var previousByteCount = 0;
        ws.on('message', function (message, flags) {
            var byteCount = ws.bytesReceived;
            var recv = byteCount - previousByteCount;
            previousByteCount = byteCount;
            hmmTime = Date.now();
            ws.send(JSON.stringify({'size': recv}));
        });
    }

    if (url === '/firewall') {
        ws.on('message', function (message) {
            if (message === "Firewall") {

                myPorts(ports);

            }
        });
    }

    function myPorts(ports) {
        var promises = [];

        for (var x = 0; x < ports.length; x++) {
            promises.push(Q.fcall(checkIfIsOpen.bind(null, [ports[x]])));
        }

        Q.all(promises).then(function (data) {
            ws.send(JSON.stringify(data));
        });
    }

    function checkIfIsOpen(args) {
        var port = args[0];
        var client = new net.Socket();
        var deferred = Q.defer();

        client.connect(port, IP, function () {
            console.log('Connected');
            client.write('Hello, server! Love, Client.');
            deferred.resolve({port: port, open: true});
        });

        client.on('data', function (data) {
            console.log('Received: ' + data);
            client.destroy(); // kill client after server's response
            deferred.resolve();
        });

        client.on('error', function (err) {
            console.log(err);
            console.log('Not Opened!');
            deferred.resolve({port: port, open: false, error:err.toString()});
        });

        client.on('close', function () {
            console.log('Connection closed');
        });

        return deferred.promise;
    }

    ws.on('close', function () {

    });
});